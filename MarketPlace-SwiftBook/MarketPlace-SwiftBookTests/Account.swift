//
//  Account.swift
//  MarketPlace-SwiftBook
//
//  Created by Дмитрий Данилин on 19.01.2023.
//

struct Account: Codable, Equatable {
    let accessToken: String
}
