//
//  RegistrationRouter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation
import UIKit

protocol RegistrationRouting: BaseRouting {}

final class RegistrationRouter {
  enum Targets {}

  private let navigationContoller: UINavigationController

  init(navigationContoller: UINavigationController) {
    self.navigationContoller = navigationContoller
  }
}

extension RegistrationRouter: RegistrationRouting {
  func routeTo(target: Any) {
    // TODO: Доделать переходы вглубь регистрации
    //guard let loginTarget = target as? RegistrationRouter.Targets else { return }
  }
}
