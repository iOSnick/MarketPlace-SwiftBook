//
//  BasketRouter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 02.04.2023.
//

import Foundation
import UIKit

protocol BasketRouting: BaseRouting {}

final class BasketRouter {
  enum Targets {
    case categoryWithProducts(id: Int)
  }

  private let navigationController: UINavigationController

  init(navigationContoller: UINavigationController) {
    self.navigationController = navigationContoller
  }
}

extension BasketRouter: BasketRouting {
  func routeTo(target: Any) {
    guard let collectionTarget = target as? BasketRouter.Targets else { return }

    switch collectionTarget {
    case .categoryWithProducts(let id):
      //let categoryViewController = BasketViewController()
      // assembly
      //navigationController.pushViewController(categoryViewController, animated: true)
      print("")
    }
  }
}
