//
//  ProductInBasketViewModel.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 02.04.2023.
//

import Foundation

struct ProductInBasketViewModel {
  let id: Int
  let productName: String
  let productCost: String
  let imageSourceURL: URL?
  let countOfThisProduct: String
}
