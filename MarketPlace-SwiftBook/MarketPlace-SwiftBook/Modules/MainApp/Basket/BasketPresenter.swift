//
//  BascetPresenter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 02.04.2023.
//

import Foundation
protocol BasketPresentation {
  func didSelectCategory(with id: Int)
}

final class BasketPresenter {
  var router: BasketRouting?
}

extension BasketPresenter: BasketPresentation {
  func didSelectCategory(with id: Int) {
    router?.routeTo(target: CollectionRouter.Targets.categoryWithProducts(id: id))
  }
}
