//
//  CollectionViewController.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 05.03.2023.
//

import UIKit

final class CollectionViewController: UIViewController {

  // MARK: VIPER
  var presenter: CollectionPresentation?

  // MARK: UI
  private let inset: CGFloat = 10
  private lazy var cellWidth: CGFloat = (self.view.frame.width - 6 * inset) / 3
  private lazy var cellHeight: CGFloat = cellWidth * 2 / 3

  // MARK: Models
  private var viewModels = [CategoryViewModel](repeating: CategoryViewModel(), count: 50)

  // MARK: Components
  @IBOutlet weak var collectionView: UICollectionView!

  override func viewDidLoad() {
    super.viewDidLoad()

    registerXibs()
    collectionView.delegate = self
    collectionView.dataSource = self
  }
}

extension CollectionViewController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    guard
      let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell,
      let id = cell.viewModel?.id
    else { return }

    presenter?.didSelectCategory(with: id)
  }
}

extension CollectionViewController: UICollectionViewDataSource {
  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    viewModels.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: String(describing: CategoryCollectionViewCell.self),
      for: indexPath
    ) as! CategoryCollectionViewCell
    cell.backgroundColor = .red
    cell.configure(with: viewModels[indexPath.row])
    return cell
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CollectionViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {

    return CGSize(width: cellWidth, height: cellHeight)
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    insetForSectionAt section: Int
  ) -> UIEdgeInsets {
    return UIEdgeInsets(top: inset, left: 1.5 * inset, bottom: inset, right: 1.5 * inset)
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    return 10
  }
}


private extension CollectionViewController {
  func registerXibs() {
    let xibNames = [
      String(describing: CategoryCollectionViewCell.self)
    ]

    xibNames.forEach { (name) in
      collectionView.register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
    }
  }
}
