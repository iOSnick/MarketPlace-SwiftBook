//
//  CollectionRouter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 05.03.2023.
//

import Foundation
import UIKit

protocol CollectionRouting: BaseRouting {}

final class CollectionRouter {
  enum Targets {
    case categoryWithProducts(id: Int)
  }

  private let navigationController: UINavigationController

  init(navigationContoller: UINavigationController) {
    self.navigationController = navigationContoller
  }
}

extension CollectionRouter: CollectionRouting {
  func routeTo(target: Any) {
    guard let collectionTarget = target as? CollectionRouter.Targets else { return }

    switch collectionTarget {
    case .categoryWithProducts(let id):
      let categoryViewController = CategoryViewController()
      // assembly
      navigationController.pushViewController(categoryViewController, animated: true)

    }
  }
}
