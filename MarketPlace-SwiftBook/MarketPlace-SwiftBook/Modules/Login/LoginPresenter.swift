//
//  LoginPresenter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation

protocol LoginPresentation {
    func runRegistratonFlow()
    func requestAuth(login: String,
                     password: String,
                     completion: @escaping (Result<Void, Error>) -> Void)
}

final class LoginPresenter {
    let router: LoginRouting
    let tokenService: IKeychainStorageService
    let api: LoginAPIConnecting

    init(router: LoginRouting, tokenService: IKeychainStorageService, api: LoginAPIConnecting) {
        self.router = router
        self.tokenService = tokenService
        self.api = api
    }
}

// MARK: - LoginPresentation
extension LoginPresenter: LoginPresentation {
    func runRegistratonFlow() {
        router.routeTo(target: LoginRouter.Targets.registration)
    }

    func requestAuth(login: String,
                     password: String,
                     completion: @escaping (Result<Void, Error>) -> Void) {

        api.requestAuth(login: login, password: password, completion: { [weak self] result in
            switch result {
            case .success(let token):
                // сохранить токен
                self?.tokenService.save(token,
                                        service: "access-token-sbmp",
                                        account: "TestLogin") { someResult in
                    switch someResult {
                    case .success:
                        completion(.success(()))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
