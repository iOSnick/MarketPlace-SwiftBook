//
//  LoginRouter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation
import UIKit

protocol LoginRouting: BaseRouting {}

final class LoginRouter {
  enum Targets {
    case registration, enter
  }

  private let navigationContoller: UINavigationController

  init(navigationContoller: UINavigationController) {
    self.navigationContoller = navigationContoller
  }
}

extension LoginRouter: LoginRouting {
  func routeTo(target: Any) {
    guard let loginTarget = target as? LoginRouter.Targets else { return }
    switch loginTarget {
    case .enter:
      print("enter to loginFlow")
    case .registration:
      print("enter to RegistrationFlow")
      let registrationScreenViewController = RegistrationScreenViewController()
      RegistrationAssembly(navigaionController: navigationContoller)
        .configure(viewController: registrationScreenViewController)
      navigationContoller.pushViewController(registrationScreenViewController, animated: true)
    }
  }
}
