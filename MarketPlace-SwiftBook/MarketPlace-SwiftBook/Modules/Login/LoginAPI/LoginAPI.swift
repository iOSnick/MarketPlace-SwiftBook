//
//  LoginAPI.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 07.05.2023.
//

import Foundation

protocol LoginAPIConnecting {
    func requestAuth(login: String,
                     password: String,
                     completion: @escaping (Result<String, Error>) -> Void)
}

final class LoginAPI {
    enum LoginAPIErrors: Error, LocalizedError {
        case badRequest
        case timeOut
    }
}

extension LoginAPI: LoginAPIConnecting {
    func requestAuth(login: String,
                     password: String,
                     completion: @escaping (Result<String, Error>) -> Void) {
        // PromiseKit
        // Alamofire
        // URLSession

        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 3) {
            DispatchQueue.main.async {
                //completion(.failure(LoginAPIErrors.badRequest))
                completion(.success("newToken"))
            }
        }
    }
}
