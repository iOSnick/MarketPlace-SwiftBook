//
//  LoginAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation
import UIKit

final class LoginAssembly {

    private let navigaionController: UINavigationController

    init(navigaionController: UINavigationController) {
        self.navigaionController = navigaionController
    }
}

extension LoginAssembly: BaseAssembly {
    func configure(viewController: UIViewController) {
        guard let loginScreenViewController = viewController as? LoginScreenViewController else { return }
        let router = LoginRouter(navigationContoller: navigaionController)
        let api = LoginAPI()
        let presenter = LoginPresenter(router: router,
                                       tokenService: KeychainStorageService.shared,
                                       api: api)

        loginScreenViewController.presenter = presenter
    }
}
