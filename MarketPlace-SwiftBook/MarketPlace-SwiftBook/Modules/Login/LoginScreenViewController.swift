//
//  LoginScreenViewController.swift
//  MarketPlace-SwiftBook
//
//  Created by Dmitry Danilin on 25.12.2022.
//

import UIKit

/// Экран логина для входа в проект
final class LoginScreenViewController: UIViewController {

    var presenter: LoginPresentation?

    @IBOutlet weak var verticalContentConstraint: NSLayoutConstraint!
    // MARK: - IBOutlets
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var enterButton: UIButton!

    @IBAction func loginDidChange(_ sender: UITextField) {
        guard let login = sender.text else { return }
        self.login = login
    }

    @IBAction func passwordDidChange(_ sender: UITextField) {
        guard let password = sender.text else { return }
        self.password = password
    }

    private var login: String = "" {
        didSet {
            checkValidation()
        }
    }
    private var password: String = "" {
        didSet {
            checkValidation()
        }
    }
    private var isChecked: Bool = false {
        didSet {
            enterButton.isEnabled = isChecked
        }
    }

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        subscribeKeyboard()
    }

    override func viewDidDisappear(_ animated: Bool) {
        unSubscribeKeyboard()
    }

    // MARK: - Override Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    // MARK: - IBActions
    @IBAction func registrationPressedButton() {
        presenter?.runRegistratonFlow()
    }

    @IBAction func enterPressedButton() {

        enterButton.isEnabled = false
        presenter?.requestAuth(login: login, password: password) { [weak self] result in
            switch result {
            case .success:
                (UIApplication.shared.delegate as? AppDelegate)?.changeRootViewController(TabBarViewController())
            case .failure(let error):
                let alertVC = UIAlertController(title: "Ошибка",
                                                message: "Что-то пошло не так:\n\(error.localizedDescription)",
                                                preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default)
                alertVC.addAction(okAction)
                self?.present(alertVC, animated: true)
            }

            self?.enterButton.isEnabled = true
        }
    }
}

// MARK: - Configuration ViewController
private extension LoginScreenViewController {

    func checkValidation() {
        isChecked = login.count > 3 && password.count > 5
    }

    func setup() {
        setupUI()
        setupTextFields()
        setupButtons()
    }

    func setupUI() {
        enterButton.isEnabled = false
    }

    func setupTextFields() {
        loginTextField.delegate = self
        passwordTextField.delegate = self
    }

    func setupButtons() {
        registrationButton.layer.borderWidth = 1
        registrationButton.layer.cornerRadius = 8
    }

    func subscribeKeyboard() {

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    func unSubscribeKeyboard() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        verticalContentConstraint.constant = -30
        UIView.animate(withDuration: 0.3, delay: 0) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        verticalContentConstraint.constant = 0
        view.layoutIfNeeded()
    }
}

// MARK: - Text Field Delegate
extension LoginScreenViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
