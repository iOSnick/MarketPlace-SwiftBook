//
//  AppDelegate.swift
//  MarketPlace-SwiftBook
//
//  Created by Сергей Вихляев on 18.12.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var keychainStorageService = ServiceAssembly().keychainStorageService

  private let tokenCheckerManager: ITokenCheckerManagement

  override init() {
    self.tokenCheckerManager = TokenCheckerManager(
      keychainStorageService: KeychainStorageService.shared
    )
    super.init()
  }

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    // 0. Замокаем данные для теста
    UserDefaults.standard.set("TestLogin", forKey: "lastLogin")

    keychainStorageService.delete(service: "access-token-sbmp", account: "TestLogin", completion: { someResult in
      print(someResult)
    })

    keychainStorageService.save("testToken", service: "access-token-sbmp", account: "TestLogin") { someResult in
      print(someResult)
    }



    // 1. Берем username(login)
    if let lastUserLogin = UserDefaults.standard.object(forKey: "lastLogin") as? String {
      // 1.1 Проверяем, есть ли токен у этого пользователя в keychain
      tokenCheckerManager.checkTokenInLocalStorage(for: lastUserLogin) { result in
        switch result {
        case .success:
          routeToMainApp()
        case .failure(let error):
          print(error.localizedDescription)
          createAndShowStartVC()
        }
      }
    }
    return true
  }

  /// Для смены рут экрана, входа и выхода из аккаунта
  /// - Parameters:
  ///   - vc: на какой экран делать переход
  ///   - animated: с анимацией переход или нет ( по умолчанию анимированный )
  func changeRootViewController(_ vc: UIViewController,
                                animated: Bool = true) {
    guard let window = window else { return }

    window.rootViewController = vc

    UIView.transition(with: window,
                      duration: 0.5,
                      options: [.showHideTransitionViews],
                      animations: nil,
                      completion: nil)
  }
}

private extension AppDelegate {

  func routeToMainApp() {
    let tabViewController = TabBarViewController()
    let mainNavigationController = UINavigationController(rootViewController: tabViewController)
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = mainNavigationController
    window?.makeKeyAndVisible()
  }

  func createAndShowStartVC() {
    let startVC = LoginScreenViewController(
      nibName: String(describing: LoginScreenViewController.self),
      bundle: nil
    )

    let navigationController = UINavigationController(rootViewController: startVC)
    let loginAssembly = LoginAssembly(navigaionController: navigationController)
    loginAssembly.configure(viewController: startVC)

    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = navigationController
    window?.makeKeyAndVisible()
  }
}
