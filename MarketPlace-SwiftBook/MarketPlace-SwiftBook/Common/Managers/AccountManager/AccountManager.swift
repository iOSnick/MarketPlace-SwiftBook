//
//  AccountManager.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 22.01.2023.
//

import Foundation

/// Протокол изменения данных об аккаунте
protocol AccountChangable: AnyObject {
  var observers: WeakCollection<AccountObservable> { get set }
  func update(accountModel newAccountModel: AccountModel, withSych isNeeded: Bool)
}

/// Протокол наблюдения за изменениями данных об аккаунте
protocol AccountObservable {
  func accountModelDidChange(_ newAccountModel: AccountModel)
}

/// Менеджер пользовательского аккаунта
final class AccountManager: AccountChangable {

  // MARK: - AccountChangable
  private var accountModel: AccountModel = AccountModel.emptyModel() {
    didSet {
      if accountModel != oldValue {
        observers.forEach {
          $0?.accountModelDidChange(accountModel)
        }
      }
    }
  }

  static let shared: AccountChangable = AccountManager()

  func update(accountModel newAccountModel: AccountModel, withSych isNeeded: Bool) {
    accountModel = newAccountModel
    if isNeeded {
      apiAccountManager.syncAccountModel(currentModel: accountModel,
                                         priority: .local) { [weak self] success, errCode, message, model in
        self?.handleAPIResult(success: success,
                              errCode: errCode,
                              message: message,
                              accountModel: model)
      }
    }
  }

  var observers = WeakCollection<AccountObservable>()

  let apiAccountManager: APIAccountManagement = APIAccountManager()

  private init() {}
}

private extension AccountManager {
  func handleAPIResult(success: Bool,
                       errCode: Int?,
                       message: String?,
                       accountModel: AccountModel?) {
    if success {
      assert(self.accountModel == accountModel)
    } else if let message = message {
      print(message)
      // Здесь стоит передать ошибку наблюдателю
    } else {
      // Здесь стоит передать неизвестную ошибку наблюдателю
    }
  }
}

