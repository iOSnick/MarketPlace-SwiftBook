//
//  AccountManagerLan.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 22.01.2023.
//

import Foundation

/// Протокол управления сетевым слоем менеджера аккаунта
protocol APIAccountManagement {

  ///  Запрос профиля
  /// - Parameter completion: handler по результату (success, message?, AccountModel?)
  func requestActualAccountModel(completion: @escaping (Bool, Int?, String?, AccountModel?)->(Void))

  /// Запрос профиля
  ///  - Parameter completion: handler по результату (success, message?, AccountModel?)
  ///  - Parameter newModel: новая модель
  func updateAccountModel(newModel: AccountModel,
                          completion: @escaping (Bool, Int?, String?, AccountModel?)->(Void))

  /// Запрос профиля
  ///  - Parameter completion: handler по результату (success, message?, AccountModel?)
  ///  - Parameter currentModel: локальная модель
  func syncAccountModel(currentModel: AccountModel,
                        priority: APIAccountManager.SyncPriority,
                        completion: @escaping (Bool, Int?, String?, AccountModel?)->(Void))
}

/// Сетевой менеджер
final class APIAccountManager {
  enum SyncPriority {
    case local
    case server
  }
}

// MARK: - APIAccountManagement
extension APIAccountManager: APIAccountManagement {
  func requestActualAccountModel(completion: @escaping (Bool, Int?, String?, AccountModel?)->(Void)) {

  }

  func updateAccountModel(newModel: AccountModel,
                          completion: @escaping (Bool, Int?, String?, AccountModel?)->(Void)) {

  }

  func syncAccountModel(currentModel: AccountModel,
                        priority: SyncPriority,
                        completion: @escaping (Bool, Int?, String?, AccountModel?) -> (Void)) {
  }
}
