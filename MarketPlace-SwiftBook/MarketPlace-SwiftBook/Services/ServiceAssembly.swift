//
//  ServiceAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Дмитрий Данилин on 19.01.2023.
//

import Foundation

/// Класс для сборки зависимостей в сервисы
final class ServiceAssembly {
    /// Сервис для работы с хранилищем Keychain
    lazy var keychainStorageService: IKeychainStorageService = {
        return KeychainStorageService.shared
    }()
}
