//
//  MockService.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 16.04.2023.
//

import Foundation

/// Протокол предоставления моковых данных
protocol Mockable {
  func accountModel() -> AccountModel
}

/// Мок-сервис для предоставления необходимых тестовых данных
final class MockService {
  static let shared: Mockable = MockService()
  private init() {}
}

// MARK: - Mockable
extension MockService: Mockable {
  func accountModel() -> AccountModel {

    let personalInfo = personalInfo()
    let orders = orders()
    let product = product()
    let settings = accountSettings()

    return AccountModel(id: -1,
                 personalInfo: personalInfo,
                 orders: orders,
                 favorites: [product],
                 settings: settings)
  }
}


private extension MockService {
  func personalInfo() -> PersonalInfo {
    PersonalInfo(nickName: "iOSnick",
                 name: "Serge",
                 email: "example@yandex.ru",
                 avatarImageURL: URL(string: "https://www.kindpng.com/picc/m/780-7804962_cartoon-avatar-png-image-transparent-avatar-user-image.png")
    )
  }

  func orders() -> [OrderModel] {
    [
      OrderModel(productsAndCount: [
        product(): 3
      ])
    ]
  }

  func product() -> Product {
    Product(id: -1,
            name: "Product",
            cost: 1000.11,
            currency: "$",
            imageSourceURL: URL(string: "https://breakingtechnews.net/wp-content/uploads/2018/12/67090155.cms.webp"))
  }

  func accountSettings() -> AccountSettings {
    AccountSettings()
  }
}
