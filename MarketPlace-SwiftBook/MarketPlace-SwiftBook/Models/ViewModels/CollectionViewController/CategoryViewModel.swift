//
//  CategoryViewModel.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 05.03.2023.
//

import Foundation

protocol IDInside {
  var id: Int? { get }
}

struct CategoryViewModel: IDInside {
  private(set) var id: Int?

  init(id: Int? = nil) {
    self.id = 1
  }
}
