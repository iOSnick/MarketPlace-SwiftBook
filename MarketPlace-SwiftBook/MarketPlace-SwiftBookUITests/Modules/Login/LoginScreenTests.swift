//
//  LoginScreenTests.swift
//  MarketPlace-SwiftBookUITests
//
//  Created by Дмитрий Данилин on 25.12.2022.
//

import XCTest

final class LoginScreenTests: XCTestCase {
    
    let app = XCUIApplication()
    
    func test_pressingReturnOnKeyboard_enterLoginTextField_keyboardHidden() {
        let returnButton = app.keyboards.buttons["Return"]
        
        app.launch()
        app.textFields["Enter login"].tap()
        returnButton.tap()
        
        XCTAssertEqual(app.keyboards.count, 0, "Клавиатура не скрылась")
    }
    
    func test_pressingReturnOnKeyboard_enterPasswordTextField_keyboardHidden() {
        let returnButton = app.keyboards.buttons["Return"]
        
        app.launch()
        app.textFields["Enter password"].tap()
        returnButton.tap()
        
        XCTAssertEqual(app.keyboards.count, 0, "Клавиатура не скрылась")
    }
    
    func test_pressedOnScreen_enterPasswordTextField_keyboardHidden() {
        app.launch()
        app.textFields["Enter password"].tap()
        tapCoordinate(x: 100, y: 100)
        
        XCTAssertEqual(app.keyboards.count, 0, "Клавиатура не скрылась")
    }
    
    fileprivate func tapCoordinate(x: Double, y: Double) {
        let normalized = app.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0))
        let coordinate = normalized.withOffset(CGVector(dx: x, dy: y))
        coordinate.tap()
    }
}
